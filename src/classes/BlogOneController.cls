global with sharing class BlogOneController {

	/*
	Sample Controller to view,create and delete account
	*/
	
	// ATRRIBUTES
	
	public static final String SUCCESS_MESSAGE 			= 'Account Successfully Updated';
	public static final String SUCCESS_MESSAGE_ONDELETE = 'Account Successfully Deleted';

    // WRAPPER CLASS

    global class ResponseClass {
        public Boolean flag {get;set;}
        public String msg {get;set;}

        public ResponseClass(Boolean b,String msg){
            this.flag = false;
            this.msg  = msg; 
        }
    }
	
	// METHODS
	
	/*
	Fetch all accounts with Name and Address
	*/
	
	@RemoteAction
    global static List<Account> getAllAccounts() {
    
    	return [SELECT Id,Name,BillingStreet,BillingCity,BillingState FROM Account];
    }
    
    
    /*Update account */
    
    @RemoteAction
    global static String updateAccount(Account account){
    	
    	try{
    	    update account;
    	    return SUCCESS_MESSAGE; 
    	}catch(Exception e){
    		return e.getMessage();
    	}
    	
    }
    
    /*
    	Create Account
    */
    
    @RemoteAction
    global static Id createAccount(Account account){
    	
    	Database.Saveresult sR = Database.insert(account);
    	
    	return sR.getId();
    }
    
    /*
    	Delete Account
    */
	
	@RemoteAction
    global static void deleteAccount(Id account){
    	try{
    		delete [SELECT Id FROM Account WHERE Id=:account];
    	}catch(Exception e){
    	 system.debug(e.getMessage());
    	}
    }   


}